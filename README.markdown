Atlassian Doc Version Maven Plugin
==================================

Usage
-----

TODO

To use the plugin, configure an execution of this plugin in your `pom.xml` (see
[Configuring Build Plugins][3]). Example:

    <plugin>
      <groupId>com.atlassian.maven.plugins</groupId>
      <artifactId>docsversion-maven-plugin</artifactId>
      <version>1.0</version>
      <executions>
        <execution>
          <id>set-docs-version-property</id>
          <phase>initialize</phase>
          <goals>
            <goal>set-property</goal>
          </goals>
          <configuration>
            <!-- the name of the property to set -->
            <property>jira.docs.version</property>
          </configuration>
        </execution>
      </executions>
    </plugin>

